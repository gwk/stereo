# Copyright 2014 George King.
# Permission to use this file is granted in license.txt.

import sys
import math
import numpy as np

def errFL(fmt, *items):
  print(fmt.format(*items), file=sys.stderr)


def error(fmt, *items):
  errFL('error: ' + fmt, *items)
  sys.exit(1)



def gauss(n=11, sigma=1):
  r = range(-int(n/2),int(n/2)+1)
  return [1 / (sigma * math.sqrt(2*math.pi)) * math.exp(-float(x)**2/(2*sigma**2)) for x in r]


def binomial_coefficients(n):
  if n == 0:
    return []
  elif n == 1:
    return [1]
  new_row = [1]
  last_row = binomial_coefficients(n-1)
  for i in range(len(last_row)-1):
    new_row.append(last_row[i] + last_row[i+1])
  new_row += [1]
  return new_row


def mk_smooth_kernel(h, w):
  # 1D binomial kernel.
  kh = binomial_coefficients(h)
  kw = binomial_coefficients(w)
  #print('kh', kh)
  #print('kw', kw)
  # 2D binomial kernel.
  k2 = np.array([[kh[i] * kw[j] for j in range(w)] for i in range(h)])
  #print('k2', k2)
  s = k2.sum()
  kn = k2 / s # normalized.
  return kn

