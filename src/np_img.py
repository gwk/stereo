# Copyright 2014 George King.
# Permission to use this file is granted in license.txt.

import numpy as _np
import scipy.misc as _spm
import os.path as _path
from util import *

u8_val_range = 1 << 8

float_types = (_np.float32, _np.float64, _np.float)

def img_to_U8(img):
  # ensure that a np image array is 8-bit.
  if img.dtype.type == _np.uint8:
    return img
  assert img.dtype.type in float_types
  scaled = img * u8_val_range
  scaled.clip(0, u8_val_range - 1, out=scaled) # clip in place.
  return scaled.astype(_np.uint8)


def img_to_F32(img):
  if img.dtype.type in float_types:
    return img
  assert img.dtype.type == _np.uint8
  f32 = img.astype(_np.float32)
  f32 /= u8_val_range - 1
  return f32


def img_to_lum(img):
  assert img.dtype.type in float_types
  assert img.shape[2] == 3
  return img[...,0] * .3 + img[...,1] * .59 + img[...,2] * .11;


def read_png(path, as_float=True):
  if not path.endswith('.png'):
    error('not a .png path: {}', path)
  img = _spm.imread(path)
  s = img.shape
  if (len(s) == 2): # no channels.
    img = img.reshape(s[0], s[1], 1)
  if as_float:
    return img_to_F32(img)
  else:
    return img


def read_npy(path, as_float=True):
  if not path.endswith('.npy'):
    error('not a .npy path: {}', path)
  img = _np.load(path)
  assert img.dtype == _np.uint8
  if (len(s) == 2): # no channels.
    img = img.reshape(s[0], s[1], 1)
  if as_float:
    return img_to_F32(img)
  else:
    return img


def read_img(path, as_float=True):
  ext = _path.splitext(path)[1]
  if ext == '.png':
    return read_png(path, as_float=as_float)
  elif ext == '.npy':
    return read_npy(path, as_float=as_float)
  else:
    error('input path has bad extension: {}', path)


def write_png(path, img):
  if not path.endswith('.png'):
    error('not a .png path: {}', path)
  #h, w, c = img.shape
  #assert c != 2 # unsupported by imsave.
  #g = (c < 3) # 1 or 2 channels indicates grayscale.
  #a = bool(c % 2 == 0) # 2 or 4 channels indicates alpha.
  u8 = img_to_U8(img)
  _spm.imsave(path, u8)


def write_npy(path, img):
  if not path.endswith('.npy'):
    error('not a .npy path: {}', path)
  if img.dtype.type in float_types:
    val_range = 1 << 8
    scaled = img * val_range
    clipped = scaled.clip(0, val_range - 1)
    img = clipped.astype(vt)
  _np.save(path, img)


def write_img(path, img):
  ext = _path.splitext(path)[1]
  if ext == '.png':
    write_png(path, img)
  elif ext == '.npy':
    write_npy(path, img)
  else:
    error('output path has bad extension: {}', path)


