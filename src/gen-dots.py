#!/usr/bin/env python3
# Copyright 2014 George King.
# Permission to use this file is granted in license.txt.

import sys
import os.path as _path
import numpy as np
import scipy.ndimage as ndi
from pprint import pprint
from np_img import *
from util import *

l = np.zeros((8, 8, 3))
r = np.zeros((8, 8, 3))

l[4, 4] = (1, 1, 1)
r[4, 3] = (1, 1, 1)

write_png('img/dotL.png', l)
write_png('img/dotR.png', r)
