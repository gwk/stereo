#!/usr/bin/env python3
# Copyright 2014 George King.
# Permission to use this file is granted in license.txt.

import sys
import os.path as _path
import numpy as np
import scipy.ndimage as ndi
from pprint import pprint
from np_img import *
from util import *

npa = np.array

smooth_k = mk_smooth_kernel(3, 3)


def smooth(img):
  h, w, c = img.shape
  # split img into color channels; flatten out the len-1 color axis via reshape.
  planes = [p.reshape(h, w) for p in np.dsplit(img, 3)]
  planes_smoothed = [ndi.convolve(p, smooth_k, origin=0, mode='reflect') for p in planes]
  smoothed = np.dstack(planes_smoothed)
  return smoothed


def down_sample(img):
  return img[::2,::2]


def down_sum(img):
  return sum([img[i::2,j::2] for i in range(2) for j in range(2)]) / 4


def gen_pyramid(path):
  base, ext = _path.splitext(path)
  errFL("reading: {}", path)
  img = read_img(path)
  n = 0
  while True:
    h, w, c = img.shape # c is number of color/alpha channels.
    assert c == 3 # for now, assume RGB.
    errFL('n:{:>2}    h:{:>4}    w:{:>4}', n, h, w)
    write_img('{}-{:02}.png'.format(base, n), img)
    if h <= 1 or w <= 1: break
    smoothed = smooth(img)
    img = down_sum(smoothed)
    n += 1



paths = sys.argv[1:]

for path in paths:
  gen_pyramid(path)

