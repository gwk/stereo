#!/usr/bin/env python3
# Copyright 2014 George King.
# Permission to use this file is granted in license.txt.

import sys
import os
import os.path as _path
import numpy as np
import scipy.ndimage as ndi
import re
from math import log2
from pprint import pprint
from np_img import *

eps = 1e-300

corr_rad_h = 2
corr_rad_w = 2

win_rad_h = 0
win_rad_w = 7

disp_k = mk_smooth_kernel(corr_rad_h * 2 + 1, corr_rad_w * 2 + 1)
disp_k_h, disp_k_w = disp_k.shape

path_fmt = '{}{}-{:>02}.png'

path_pre, base_level_str = sys.argv[1:]
name_pre = _path.split(path_pre)[-1]
base_level = int(base_level_str)

#print(path_pre, name_pre, base_level)

def load_images():
  file_re = re.compile(r'{}[LR]-\d+.png'.format(name_pre))
  path_l = path_fmt.format(path_pre, 'L', base_level)
  path_r = path_fmt.format(path_pre, 'R', base_level)

  path_dir = _path.dirname(path_pre)
  dir_contents = os.listdir(path_dir)
  names = [s for s in dir_contents if file_re.fullmatch(s)]

  assert (len(names) % 2 == 0)
  names_l = names[:len(names) // 2]
  names_r = names[len(names) // 2:]

  name_pairs = list(zip(names_l, names_r))
  #pprint(name_pairs)
  
  def img(n):
    return read_img(_path.join(path_dir, n))

  return [(img(l), img(r)) for (l, r) in name_pairs]


def norm_img(img, f):
  img_min = np.amin(img)
  img_max = np.amax(img)
  img_rng = img_max - img_min
  f_min = f(img_min) if f else img_min
  f_max = f(img_max) if f else img_max
  f_rng = f_max - f_min
  errFL('img min:{} max:{} rng:{} f min:{} max:{} rng:{}',
    img_min, img_max, img_rng, f_min, f_max, f_rng)

  assert 0 < img_min < img_max
  out = np.array(img)
  if f:
    f(out, out=out)
  np.subtract(out, f_min, out=out)
  np.divide(out, f_rng, out=out)
  return out


def offset_img(offs, scale):
  # expects shape of (h, w).
  img = offs.astype(np.float32)
  np.add(img, scale, out=img)
  np.divide(img, scale * 2, out=img)
  return img


def disparity(a, b, weights):
  return (((a - b) * weights) ** 2).sum() / weights.sum()


def disp_search(lum_l, lum_r, h, w, i, j):
  d_min = 1000000 # stupidly large.
  c_min = (0, 0)
  n_min = 0
  for k in range(-win_rad_h, win_rad_h + 1):
    ir = i + k
    oh_lo = -corr_rad_h             # offset height low.
    oh_hi = corr_rad_h + 1          # offset height high.
    oh_lo -= min(0, i + oh_lo, ir + oh_lo) # clamp to low edge.
    oh_hi += min(0, h - i - oh_hi, h - ir - oh_hi) # clamp to high edge.
    if oh_lo >= oh_hi: continue
    il_lo = i + oh_lo
    il_hi = i + oh_hi
    ir_lo = ir + oh_lo
    ir_hi = ir + oh_hi
    ic_lo = corr_rad_h + oh_lo
    ic_hi = corr_rad_h + oh_hi
    assert 0 <= il_lo < il_hi <= h
    assert 0 <= ir_lo < ir_hi <= h
    assert 0 <= ic_lo < ic_hi <= disp_k_h
    for l in range(-win_rad_w, win_rad_w + 1):
      jr = j + l
      ow_lo = -corr_rad_w             # offset width low.
      ow_hi = corr_rad_w + 1          # offset width high.
      ow_lo -= min(0, j + ow_lo, jr + ow_lo) # clamp to low edge.
      ow_hi += min(0, w - j - ow_hi, w - jr - ow_hi) # clamp to high edge.
      if ow_lo >= ow_hi: continue
      jl_lo = j + ow_lo
      jl_hi = j + ow_hi
      jr_lo = jr + ow_lo
      jr_hi = jr + ow_hi
      jc_lo = corr_rad_w + ow_lo
      jc_hi = corr_rad_w + ow_hi
      #errFL("w:{} j:{} l:{} ow_lo:{} ow_hi:{} jl_lo:{}, jl_hi:{}",
      #  w, j, l, ow_lo, ow_hi, jl_lo, jl_hi)
      #errFL("jc_lo:{} jc_hi:{} disp_k_w:{}", jc_lo, jc_hi, disp_k_w)
      assert 0 <= jl_lo < jl_hi <= w
      assert 0 <= jr_lo < jr_hi <= w
      assert 0 <= jc_lo < jc_hi <= disp_k_w

      patch_l =  lum_l[il_lo:il_hi, jl_lo:jl_hi]
      patch_r =  lum_r[ir_lo:ir_hi, jr_lo:jr_hi]
      patch_k = disp_k[ic_lo:ic_hi, jc_lo:jc_hi]
      d = disparity(patch_l, patch_r, patch_k)
      if d < d_min:
        d_min = d
        c_min = (k, l)
        n_min = 1
      elif d == d_min:
        n_min += 1

  return c_min, n_min


images = load_images()
#pprint(images)


def err_img(label, img):
  errFL('\n{}:', label)
  print(img)


def build_maps(level):
  img_l, img_r = images[level]

  assert img_l.shape == img_r.shape

  h, w, c = img_l.shape
  path_d = path_fmt.format(path_pre, 'D', level)

  errFL('build_maps:{} h:{} {}', path_d, h, w)

  lum_l = img_to_lum(img_l)
  lum_r = img_to_lum(img_r)
  #err_img('lum l', lum_l)
  #err_img('lum r', lum_r)

  disp_offs = np.zeros((h, w), dtype=np.int16)
  disp_ns = np.zeros((h, w), dtype=np.int16)

  for i in range(h):
    for j in range(w):
      c_min, n_min = disp_search(lum_l, lum_r, h, w, i, j)
      disp_offs[i][j] = c_min[1] # if n_min == 1 else 9
      disp_ns[i][j] = n_min

  #err_img('disp_offs', disp_offs)
  #err_img('disp_ns', disp_ns)
  offs_img = offset_img(disp_offs, win_rad_w)
  #disp_norm_log = norm_img(disp, np.sqrt)
  write_img(path_d, offs_img)


for level in range(len(images) - 1, base_level - 1, -1):
  build_maps(level)

